
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoGet
        go_bin: null
        options: null

        constructor: () ->
            @element = document.createElement 'atom-panel'
            @element.classList.add 'modal'

            div_panel = document.createElement 'div'
            div_panel.classList.add 'inset-panel'
            @element.appendChild div_panel

            div_header = document.createElement 'div'
            div_header.classList.add 'panel-heading'
            div_header.textContent = 'AtomGo Installer'
            div_panel.appendChild div_header

            div_content = document.createElement 'div'
            div_content.classList.add 'panel-body'
            div_content.classList.add 'padded'
            div_panel.appendChild div_content

            div_log = document.createElement 'div'
            div_log.classList.add 'inset-panel'
            div_log.classList.add 'padded'
            div_content.appendChild div_log
            div_content.appendChild document.createElement 'br'

            @log = log = document.createElement 'ul'
            log.classList.add 'info-messages'
            log.classList.add 'block'
            div_log.appendChild log

            button_goget = document.createElement 'button'
            button_goget.classList.add 'btn'
            button_goget.classList.add 'btn-primary'
            button_goget.classList.add 'icon'
            button_goget.classList.add 'icon-cloud-download'
            button_goget.classList.add 'inline-block-tight'
            button_goget.textContent = 'Run go get'
            button_goget.onclick = () =>
                while log.firstChild then log.removeChild log.firstChild
                @go_get()
            div_content.appendChild button_goget

            button_close = document.createElement 'button'
            button_close.classList.add 'btn'
            button_close.textContent = 'Close'
            button_close.onclick = () =>
                while log.firstChild then log.removeChild log.firstChild
                @panel.hide()
            div_content.appendChild button_close

            @panel = atom.workspace.addModalPanel(item: @element, visible: false)

        show: () =>
            if !@panel.isVisible()
                @panel.show()

        set_configuration: (configuration) ->
            if !configuration?
                @go_bin = null
                return

            @go_bin = path.join configuration.env["GOROOT"], 'bin', 'go'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
            @module_path = configuration.module_path
            @options.cwd = @module_path
            @save_before_build = configuration.save_before_build

        go_get: () =>
            return if !@go_bin?
            if @save_before_build
                for editor in atom.workspace.getTextEditors()
                    editor.save()
            proc = child_process.spawn @go_bin, ['get'], @options
            @logString '> go get'
            proc.stdout.on 'data', (data) =>
                @logString data.toString()
            proc.stderr.on 'data', (data) =>
                @logString data.toString()
            proc.on 'close', (code) =>
                @logString 'go get process close with code ', code

        logString: (str) =>
            li = document.createElement 'li'
            li.textContent = str
            @log.appendChild li

        destroy: () =>
            @element.remove()
            @panel.destroy()

    module.exports = GoGet
