
    {CompositeDisposable} = require 'atom'

    path = require 'path'
    fs = require 'fs'
    Configurator = require './config'
    StatusBar = require './statusbar'
    GoCode = require './gocode'
    GoGuru = require './goguru'
    GoFmt = require './gofmt'
    GoBuild = require './gobuild'
    GoInstall = require './goinstall'
    GoGet = require './goget'
    GoMenu = require './menu'
    GoInstaller = require './installer'
    RunTarget = require './runtarget'
    ConsoleView = require './console'

    Panel = require './panel'

    class AtomgoMain
        configurator: null
        gocodeprovider: null
        goguru: null
        gofmt: null
        gobuild: null
        goinstall: null
        goget: null
        runtarget: null
        statusBar: null
        subscriptions: null

        menu: null

        panel: null

        constructor: () ->
            @statusBar = new StatusBar()
            @menu = new GoMenu()

            @panel = new Panel @statusBar.panel_btn

            @gocodeprovider = new GoCode()
            @goguru = new GoGuru()
            @gofmt = new GoFmt()
            @gobuild = new GoBuild @panel, @statusBar.build
            @goinstall = new GoInstall @panel, @statusBar.install
            @goget = new GoGet()
            @runtarget = new RunTarget @statusBar.run

            callbacks =
                reload_configuration_file: @reload_configuration_file
                set_configuration: @set_configuration
            @configurator = new Configurator @statusBar.configurations, callbacks
            @reload_configuration_file()

        reload_configuration_file: () =>
            res = @configurator.load_project_configuration()
            if !res.error?
                @set_configuration 0

        set_configuration: (config_index) =>
            configuration = @configurator.set_config config_index
            try
                stats = fs.statSync configuration.module_path
            catch error
                options =
                    detail: "'module_path' ('" + configuration.module_path + "') in configuration '" + configuration.name + "' is not exist.\n" + error
                    dismissable: true
                atom.notifications.addError "Error select configuration '" + configuration.name + "'.", options
                return

            if !stats.isDirectory()
                options =
                    detail: "'module_path' (" + configuration.module_path + ") in configuration '" + configuration.name + "' is not directory"
                    dismissable: true
                atom.notifications.addError "Error select configuration '" + configuration.name + "'.", options
                return

            options =
                detail: "Configuration '" + configuration.name + "' selected!"
            atom.notifications.addSuccess "AtomGoGoGo", options

            if configuration?
                console.log "set config: ", configuration.name
                @statusBar.set_configuration configuration
                @menu.set_configuration configuration
                @gocodeprovider.set_configuration configuration
                @goguru.set_configuration configuration
                @gofmt.set_configuration configuration
                @gobuild.set_configuration configuration
                @goinstall.set_configuration configuration
                @goget.set_configuration configuration
                @runtarget.set_configuration configuration

        activate: (state) =>
            @consoleView = new ConsoleView(state.consoleViewState)
            @runtarget.logString = (str) =>
                @consoleView.log str
            # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
            @subscriptions = new CompositeDisposable
            # Register command that toggles this view
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:build': =>
                @gobuild.build()
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:install': =>
                @goinstall.build()
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:get': =>
                @goget.show()
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:run': =>
                @runtarget.show()
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:installer': ->
                new GoInstaller().ShowInstaller()
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:show_project_file': =>
                atom.workspace.open @configurator.config_path
            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:reload_configuration_file': =>
                @reload_configuration_file()

            @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:whicherrs': =>
                @goguru.whicherrs()

        provideAutocomplete: =>
            @gocodeprovider

        consumeStatusBar: (statusBar) =>
            @statusBar.set statusBar

        consumeLinter: (indieRegistry) =>
            @gobuild.linter = indieRegistry.register {name: 'GoGoGo Linter'}
            #@goinstall.linter = indieRegistry.register {name: 'GoGoGoInstall Linter'}
            @subscriptions.add @gobuild.linter
            #@subscriptions.add @goinstall.linter

        deactivate: () ->
            @statusBar.deactivate()
            @subscriptions.dispose()
            @panel.deactivate()

    module.exports = new AtomgoMain()
