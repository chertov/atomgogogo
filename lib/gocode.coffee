
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoCode
        selector: '.source.go'
        disableForSelector: '.source.go .comment'

        # This will take priority over the default provider, which has a priority of 0.
        # `excludeLowerPriority` will suppress any providers with a lower priority
        # i.e. The default provider will be suppressed
        inclusionPriority: 100
        excludeLowerPriority: true

        gocode_bin: null
        options: null

        set_configuration: (configuration) ->
            @gocode_bin = path.join configuration.env["GOROOT"], 'bin', 'gocode'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
            @options.cwd = configuration.module_path
            gocode_proc_config = child_process.spawn @gocode_bin, ['set', 'lib-path', @options.env['PKGDIR']], @options
            gocode_proc_config.on 'close', (code) ->

        gocode: (text, offset, callback) =>
            # proc = child_process.spawn @gocode_bin, ['-f=json', '--in=' + filepath, 'autocomplete', offset], @options
            proc = child_process.spawn @gocode_bin, ['-f=json', 'autocomplete', offset], @options
            stdout = ""
            proc.stdout.on 'data', (data) ->
                stdout += data.toString()
                #console.log 'stdout: ' + data.toString()
            proc.stderr.on 'data', (data) ->
                #console.log 'gocode stderr: ' + data.toString()
            proc.on 'error', (err) ->
                #console.log 'gocode process error ', err
                callback stdout
            proc.on 'message', (message) ->
                #console.log 'gocode process message  ', message
            proc.on 'exit', (code, signal) ->
                #console.log 'gocode process exit with code ', code, ' signal "', signal , '"'
            proc.on 'disconnect', (code) ->
                #console.log 'gocode process disconnect ', code
            proc.on 'close', (code, signal) ->
                #console.log 'gocode process close with code ', code, ' signal "', signal , '"'
                callback stdout

            proc.stdin.setEncoding 'utf-8'
            proc.stdin.write text
            proc.stdin.end()

        # Required: Return a promise, an array of suggestions, or null.
        getSuggestions: ({editor, bufferPosition, scopeDescriptor, prefix, activatedManually}) =>
            return null if !@gocode_bin?
            new Promise (resolve) =>
                characterOffset = editor.getBuffer().characterIndexForPosition(editor.getCursorBufferPosition())
                fulltext = editor.getText()
                text = fulltext.substring(0, characterOffset)
                offset = Buffer.byteLength(text, "utf8")
                filepath = editor.getPath()
                @gocode fulltext, offset, (gocode_out_str) ->
                    gocode_out = JSON.parse(gocode_out_str)
                    if !gocode_out?
                        resolve []
                        return
                    if !Array.isArray(gocode_out)
                        resolve []
                        return
                    if gocode_out.length < 2
                        resolve []
                        return
                    pos = gocode_out[0]
                    gocode_suggestions = gocode_out[1]

                    # Find your suggestions here
                    suggestions = []
                    for gocode_suggestion in gocode_suggestions
                        suggestion =
                            text: gocode_suggestion.name # OR
                            snippet: gocode_suggestion.name #.substring pos
                            displayText: gocode_suggestion.name # (optional)

                        switch gocode_suggestions.class
                            when 'func' then suggestion.type = 'function'
                            when 'var' then suggestion.type = 'variable'
                            when 'const' then suggestion.type = 'constant'
                            when 'PANIC' then suggestion.type = 'panic'

                        suggestions.push suggestion
                    resolve suggestions

        # (optional): called _after_ the suggestion `replacementPrefix` is replaced
        # by the suggestion `text` in the buffer
        onDidInsertSuggestion: ({editor, triggerPosition, suggestion}) ->
            # console.log 'insert', suggestion.text

        # (optional): called when your provider needs to be cleaned up. Unsubscribe
        # from things, kill any processes, etc.
        dispose: ->

    module.exports = GoCode
