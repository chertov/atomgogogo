
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoFmt
        gofmt_bin: null
        options: null
        enable: false

        constructor: ->
            atom.workspace.observeTextEditors @observeTextEditors

        set_configuration: (configuration) ->
            @enable = configuration.gofmt_enable
            @gofmt_bin = path.join configuration.env["GOROOT"], 'bin', 'gofmt'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
            @options.cwd = configuration.module_path

        observeTextEditors: (editor) =>
            grammar = editor?.getGrammar()
            return if !grammar or grammar.name != 'Go'
            editor.onDidSave (event) =>
                @gofmt editor.getPath()

        gofmt: (filepath) =>
            return if !@enable
            proc = child_process.spawn @gofmt_bin, ['-d', '-e', '-w', filepath], @options
            proc.stdout.on 'data', (data) ->
                console.log 'gofmt stdout: ' + data.toString()
            proc.stderr.on 'data', (data) ->
                console.log 'gofmt stderr: ' + data.toString()
            proc.on 'close', (code) ->
                console.log 'gofmt process exited with code ', code

    module.exports = GoFmt
