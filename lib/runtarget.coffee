
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class RunTarget
        bin: null
        options: null
        status_bar: null

        constructor: (status_bar) ->
            @status_bar = status_bar

            @element = document.createElement 'atom-panel'
            @element.classList.add 'modal'

            div_panel = document.createElement 'div'
            div_panel.classList.add 'inset-panel'
            @element.appendChild div_panel

            div_header = document.createElement 'div'
            div_header.classList.add 'panel-heading'
            div_header.textContent = 'AtomGo Target Runner'
            div_panel.appendChild div_header

            div_content = document.createElement 'div'
            div_content.classList.add 'panel-body'
            div_content.classList.add 'padded'
            div_panel.appendChild div_content

            div_log = document.createElement 'div'
            div_log.classList.add 'inset-panel'
            div_log.classList.add 'padded'
            div_log.classList.add 'atomgo__column'
            div_content.appendChild div_log
            div_content.appendChild document.createElement 'br'

            @log = log = document.createElement 'ul'
            log.classList.add 'info-messages'
            log.classList.add 'block'
            div_log.appendChild log

            button_kill = document.createElement 'button'
            button_kill.classList.add 'btn'
            button_kill.classList.add 'btn-primary'
            button_kill.classList.add 'icon'
            button_kill.classList.add 'icon-cloud-download'
            button_kill.classList.add 'inline-block-tight'
            button_kill.textContent = 'Stop process'
            button_kill.onclick = () =>
                @try_to_kill()
            div_content.appendChild button_kill

            button_close = document.createElement 'button'
            button_close.classList.add 'btn'
            button_close.textContent = 'Close'
            button_close.onclick = () =>
                while log.firstChild then log.removeChild log.firstChild
                @panel.hide()
            div_content.appendChild button_close

            @panel = atom.workspace.addModalPanel(item: @element, visible: false)

        show: () =>
            #if !@panel.isVisible()
            #    @panel.show()
            @run()

        set_configuration: (configuration) ->
            if !configuration?
                @bin = null
                return
            @bin = configuration.target_path
            @options = {env: process.env}
            
            @status_bar.classList.remove 'hidden'
            @stop_exec_status()

        run: () =>
            console.log @bin
            return if !@bin?
            @run_exec_status()
            @proc = proc = child_process.spawn @bin, [], @options
            @logString '> run target'
            proc.stdout.on 'data', (data) =>
                @logString data.toString()
            proc.stderr.on 'data', (data) =>
                @logString data.toString()
            proc.on 'error', (err) =>
                @logString 'target process error ', err
                console.log 'target process error ', err
                @stop_exec_status()
            proc.on 'close', (code) =>
                @logString 'target process close with code ', code
                @stop_exec_status()

        try_to_stop: () =>
            @proc.kill 'SIGINT'

        run_exec_status: () =>
            return if !@status_bar?
            while @status_bar.firstChild then @status_bar.removeChild @status_bar.firstChild
            stop_span = document.createElement 'span'
            stop_span.classList.add 'inline-block'
            stop_span.classList.add 'icon'
            stop_span.classList.add 'icon-primitive-square'
            stop_span.addEventListener "click", () =>
                @try_to_stop()
            @status_bar.appendChild stop_span
            # @panel.compiler_output.clear()
            # @errorsView.clear()

        stop_exec_status: () =>
            return if !@status_bar?
            while @status_bar.firstChild then @status_bar.removeChild @status_bar.firstChild
            run_span = document.createElement 'span'
            run_span.classList.add 'inline-block'
            run_span.classList.add 'icon'
            run_span.classList.add 'icon-playback-play'
            run_span.addEventListener "click", () =>
                @run()
            @status_bar.appendChild run_span

        logString: (str) =>
            li = document.createElement 'li'
            li.textContent = str
            @log.appendChild li

        destroy: () =>
            @element.remove()
            @panel.destroy()

    module.exports = RunTarget
