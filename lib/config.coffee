
    fs = require 'fs'
    path = require 'path'
    os = require 'os'

    calc_string = (value, env, global_env, system_env) ->
        re = /(?:%([a-zA-Z0-9_]+?)%)/gm
        vars_in_value = value.match re
        if vars_in_value?
            for _var, index in vars_in_value
                _var = _var.replace /%/gm, ''
                _var_value = get_value(_var, env, global_env, system_env)
                if _var_value?
                    value = value.replace ('%' + _var + '%'), _var_value
        return value

    get_value = (name, env, global_env, system_env) ->
        value = env[name]
        if !value?
            value = global_env[name]
            if !value?
                value = system_env[name]
                if !value?
                    console.log 'Critical error: can\'t find env: ' + name
                    return undefined
        return calc_string value, env, global_env, system_env

    make_absolutle_path = (root, path_str) ->
        root = path.normalize root
        path_str = path.normalize path_str
        if !path.isAbsolute(path_str)
            return path.join root, path_str
        return path_str

    load_config = (config_path) ->
        json = fs.readFileSync config_path
        try
            cfg = JSON.parse json
        catch error
            return {error: "Can't parse\n" + config_path + "\n" + error}
        cfg.env = {} if !cfg.env?

        project_path = path.dirname config_path
        if !cfg.project_name?
            return {error: "Can't find \"project_name\" in \n    " + config_path}
        cfg.project_path = project_path
        cfg.env['PROJECT_NAME'] = cfg.project_name
        cfg.env['PROJECT_PATH'] = cfg.project_path

        cfg.gofmt_enable = true if !cfg.gofmt_enable?

        if !cfg.env['GOOS']?
            if os.type() == "Windows_NT"
                cfg.env['GOOS'] = "windows"
            else
                if os.type() == "Linux"
                    cfg.env['GOOS'] = "linux"
                else
                    if os.type() == "Darwin"
                        cfg.env['GOOS'] = "darwin"
                    else
                        return {error: "Can't detect os type!  os.type() return " +  os.type()}

        if !cfg.env['GOARCH']?
            if os.arch() == "x64"
                cfg.env['GOARCH'] = "amd64"
            else
                if os.arch() == "ia32"
                    cfg.env['GOARCH'] = "386"
                else
                    return {error: "Can't detect os arch! os.arch() return " +  os.arch()}

        if !cfg.env['GOROOT']?
            isWin = /^win/.test process.platform
            console.log 'isWin:', isWin
            if isWin
                cfg.env['GOROOT'] = "C:\\Go\\"
            else
                cfg.env['GOROOT'] = "/usr/local/go/"

        cfg.env['GOPATH'] = "./" if !cfg.env['GOPATH']?
        cfg.env['PKGDIR'] = "./pkg/%GOOS%_%GOARCH%/" if !cfg.env['PKGDIR']?

        cfg.save_before_build = true if !cfg.save_before_build?
        cfg.gofmt_enable = true if !cfg.gofmt_enable?

        if !cfg.configurations?
            return {error: "Can't find field \"configurations\" (configuration list) in: \n" + config_path}
        if !Array.isArray(cfg.configurations)
            return {error: "\"configurations\" isn't array in: \n" + config_path}
        if cfg.configurations.length == 0
            return {error: "\"configurations\" (configuration list) is empty in: \n" + config_path}
        for configuration, cfg_index in cfg.configurations
            # copy value from global env if not exists
            for key, value of cfg.env
                cfg.configurations[cfg_index].env[key] = value if !cfg.configurations[cfg_index].env[key]?

            if !configuration.name?
                configuration.name = 'Configuration #' + cfg_index
            if !configuration.module_path?
                return {error: "Can't find \"module_path\" in configuration " + cfg_index + " in: \n" + config_path}
            if !configuration.target_path?
                cfg.configurations[cfg_index].target_path = "./bin/" + cfg.project_name

            configuration.gofmt_enable = cfg.gofmt_enable if !configuration.gofmt_enable?
            configuration.save_before_build = cfg.save_before_build if !configuration.save_before_build?

            cfg.configurations[cfg_index].env['TARGET_PATH'] = cfg.configurations[cfg_index].target_path
            cfg.configurations[cfg_index].env['MODULE_PATH'] = cfg.configurations[cfg_index].module_path

        for configuration, cfg_index in cfg.configurations
            # calc %var% variables
            for _var, value of configuration.env
                cfg.configurations[cfg_index].env[_var] = get_value _var, configuration.env, cfg.env, process.env

            # make absolutle paths
            configuration.env['GOROOT'] = make_absolutle_path cfg.project_path, configuration.env['GOROOT']
            configuration.env['MODULE_PATH'] = make_absolutle_path cfg.project_path, configuration.env['MODULE_PATH']
            configuration.env['PKGDIR'] = make_absolutle_path cfg.project_path, configuration.env['PKGDIR']
            configuration.env['TARGET_PATH'] = make_absolutle_path cfg.project_path, configuration.env['TARGET_PATH']

            if configuration.build_args?
                configuration.build_args = calc_string configuration.build_args, configuration.env, cfg.env, process.env

            configuration.target_path = configuration.env['TARGET_PATH']
            configuration.module_path = configuration.env['MODULE_PATH']

            # calc GOPATHs
            gopaths = configuration.env['GOPATH'].split ";"
            for gopath, index in gopaths
                gopaths[index] = make_absolutle_path(cfg.project_path, gopath + "/")
            gopaths.push make_absolutle_path(cfg.project_path, "./")
            gopaths.push make_absolutle_path(configuration.module_path, "./")
            if os.type() == "Windows_NT"
                configuration.env['GOPATH'] = gopaths.join ";"
            else
                configuration.env['GOPATH'] = gopaths.join ":"

        # console.log cfg
        # value = @get_value 'PKGDIR', cfg.configurations[0].env, cfg.env, process.env
        # console.log 'PKGDIR', value
        # value = @get_value 'GOARCH11', cfg.configurations[0].env, cfg.env, process.env
        # console.log 'GOARCH11', value

        if cfg.debug_cfg_out_path?
            dbg_cfg_path = cfg.debug_cfg_out_path
            if !path.isAbsolute cfg.debug_cfg_out_path
                dbg_cfg_path = path.join project_path, cfg.debug_cfg_out_path
            console.log 'Save debug config to ', dbg_cfg_path
            fs.writeFileSync dbg_cfg_path, JSON.stringify(cfg, null, 4)

        return {cfg: cfg, error: null}

    class Configurator
        project_configuration: null
        config_path: null
        selected_configuration: null

        status_bar_selector: null

        constructor: (status_bar, callbacks) ->
            @status_bar_selector = status_bar.selector
            status_bar.selector.addEventListener "change", () ->
                callbacks.set_configuration @selectedIndex
            status_bar.reload.classList.add 'icon'
            status_bar.reload.classList.add 'icon-sync'
            status_bar.reload.addEventListener "click", () ->
                callbacks.reload_configuration_file()

        status_bar_clear: () =>
            while @status_bar_selector.firstChild then @status_bar_selector.removeChild @status_bar_selector.firstChild
            @status_bar_selector.classList.add 'hidden'

        status_bar_update: () =>
            if @project_configuration?
                @status_bar_clear()
                for conf in @project_configuration.configurations
                    option = document.createElement 'option'
                    option.textContent = conf.name
                    @status_bar_selector.appendChild option
                @status_bar_selector.classList.remove 'hidden'

        load_project_configuration: () ->
            @status_bar_clear()

            paths = atom.project.getPaths()
            config_paths = []
            for project_path in paths
                tmp_path = path.join project_path, 'atomgo.json'
                if fs.existsSync(tmp_path)
                    config_paths.push tmp_path

            if config_paths.length == 0
                message = "Can't find 'atomgo.json' in:"
                for project_path in paths
                    message += '\n    ' + project_path
                message += '\n' + 'Use right click on folder and AtomGoGoGo'
                console.log message
                options =
                    detail: message
                atom.notifications.addWarning "AtomGoGoGo", options
                return {error: true}

            if config_paths.length > 1
                message = "Project have few \'atomgo.json\' configs:"
                for config_path in config_paths
                    message += '\n    ' + config_path
                options =
                    detail: message
                    dismissable: true
                atom.notifications.addFatalError "AtomGoGoGo project configuration error", options
                return {error: true}

            if config_paths.length == 1
                config_path = config_paths[0]
                config = load_config config_path
                if config.error?
                    options =
                        detail: 'Error load config.\n' + config.error
                        dismissable: true
                    atom.notifications.addError "AtomGoGoGo project configuration error", options
                    return config
                else
                    @project_configuration = config.cfg
                    @config_path = config_path
                    options =
                        detail: 'Load config from ' + config_path
                    atom.notifications.addSuccess "AtomGoGoGo project configuration initialized", options
                    @status_bar_update()
                    return config
            return null

        set_config: (config_index) =>
            if !@project_configuration?
                options =
                    detail: "Configuration not loaded"
                    dismissable: true
                atom.notifications.addFatalError "AtomGoGoGo project configuration error", options

            if config_index >= @project_configuration.configurations.length
                options =
                    detail: "Configurations count less then " + (config_index + 1) + " in " + config_path
                    dismissable: true
                atom.notifications.addFatalError "AtomGoGoGo project configuration error", options

            @selected_configuration = @project_configuration.configurations[config_index]

            return @selected_configuration

    module.exports = Configurator
