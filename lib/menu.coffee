
    class GoMenu
        constructor: () ->
            @menu =
                label: 'GoGoGo!!!'
                submenu: [
                    { label: 'Go build', command: 'atomgo:build'}
                    { label: 'Go get', command: 'atomgo:get'}
                    { label: 'Settings', command: 'atomgo:settings' }
                    { label: 'Install tools', command: 'atomgo:installer' }
                    { type: 'separator' }
                    { label: 'Generate Project file', command: 'atomgo:generate_project_file' }
                    { label: 'Show Project file', command: 'atomgo:show_project_file' }
                    { label: 'Reload Project file', command: 'atomgo:show_project_file' }
                ]

        set_configuration: (configuration) =>
            return if !configuration?
            return if @main_menu?
            @main_menu = atom.menu.add [@menu]

        deactivate: ->

    module.exports = GoMenu
