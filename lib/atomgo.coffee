ErrorsView = require './errors-view'
AtomgoConfig = require './config'
AtomgoInstaller = require './installer'
GoCode = require './gocode'
GoDef = require './godef'
{CompositeDisposable} = require 'atom'

fs = require 'fs'
path = require 'path'
child_process = require 'child_process'

class Atomgo
    errorsView: null
    errorsPanel: null
    alt_down: false
    subscriptions: null
    gocodeprovider: null
    godef: null

    constructor: () ->
        @gocodeprovider = new GoCode()
        @godef = new GoDef()

    update_config: () =>
        cfg = (new AtomgoConfig()).load_config()
        @configuration = cfg.configurations[0]
        @gocodeprovider.set_configuration @configuration
        @godef.set_configuration @configuration

    activate: (state) =>
        @errorsView = new ErrorsView(state.atomgoViewState)
        @errorsPanel = atom.workspace.addBottomPanel(item: @errorsView.getElement(), visible: true)

        # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
        @subscriptions = new CompositeDisposable

        # Register command that toggles this view
        @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:build': => @go_build()
        @subscriptions.add atom.commands.add 'atom-workspace', 'atomgo:toggle': => @go_build()

        document.body.addEventListener "keyup", (event) => @alt_down = event.altKey
        document.body.addEventListener "keydown", (event) => @alt_down = event.altKey
        atom.workspace.observeTextEditors @observeTextEditors

        @errorsPanel.show()

        @update_config()

        # console.log 'View', View
        # # When you want to add it
        # view = new MyView("Go Go GO!")
        # atom.workspace.statusBar.appendLeft(view)

    observeTextEditors: (editor) =>
        editor.onDidChangeCursorPosition (event) =>
            @on_cursor event, editor

    on_cursor: (event, editor) =>
        return if !@alt_down
        grammar = editor?.getGrammar()
        return if !grammar or grammar.name != 'Go'
        characterOffset = editor.getBuffer().characterIndexForPosition(editor.getCursorBufferPosition())
        fulltext = editor.getText()
        text = fulltext.substring(0, characterOffset)
        offset = Buffer.byteLength(text, "utf8")
        # console.log editor.getTitle() + ": ", editor.getText().substring(characterOffset - 10, characterOffset)
        @godef.godef fulltext, offset
    provide: =>
        @gocodeprovider
    deactivate: ->
        @errorsPanel.destroy()
        @subscriptions.dispose()
        @errorsView.destroy()

    serialize: ->
        errorsViewState: @errorsView.serialize()

    go_build: ->
        # new AtomgoInstaller().ShowInstaller "C:\\Go\\"
        options = {env: process.env}
        for _var, value of @configuration.env
            options.env[_var] = value
        options.cwd = options.env['MODULE_PATH']
        target_path = options.env['TARGET_PATH']
        pkg_path = options.env['PKGDIR']
        ls = child_process.spawn 'go', ['build', '-i', '-o', target_path, '-pkgdir', pkg_path], options

        all_str = ""
        ls.stdout.on 'data', (data) ->
            all_str += data
            console.log 'stdout: ' + data
        ls.stderr.on 'data', (data) ->
            all_str += data
            console.log 'stderr: ' + data
        ls.on 'close', (code) =>
            @errorsPanel.show()
            @errorsView.setStdOut all_str, @configuration
            console.log 'go build process exited with code ', code

module.exports = new Atomgo()
