path = require 'path'

module.exports =
class ErrorsView
    constructor: (element) ->
        @element = element

    onClickError: (id) ->
        err = @errors[id]
        if err?
            options =
                initialLine: err.line - 1
                initialColumn: err.pos - 1
            atom.workspace.open err.path, options

    clear: () =>
        @element.innerHTML = ""

    setStdOut: (errors) =>
        @errors = errors

        ol_dom = document.createElement 'ol'
        ol_dom.classList.add 'atomgo__errors-list'

        for log_err, i in @errors
            li_dom = document.createElement 'li'
            li_dom.classList.add 'atomgo__error'

            li_dom_module = document.createElement 'span'
            li_dom_module.classList.add 'atomgo__error-module'
            li_dom_module.textContent = log_err.module + ' '

            li_dom_path = document.createElement 'span'
            li_dom_path.classList.add 'atomgo__error-path'
            li_dom_path.textContent = log_err.path + ' : '

            li_dom_line = document.createElement 'span'
            li_dom_line.classList.add 'atomgo__error-line'
            li_dom_line.textContent = log_err.line + ' : '

            li_dom_pos = document.createElement 'span'
            li_dom_pos.classList.add 'atomgo__error-pos'
            li_dom_pos.textContent = log_err.pos

            li_dom_message = document.createElement 'span'
            li_dom_message.classList.add 'atomgo__error-message'
            li_dom_message.textContent = ' ' + log_err.message

            #li_dom.textContent = log_err.module + "   " + log_err.path + ":" + log_err.line + ":" + log_err.pos + log_err.message
            li_dom.appendChild li_dom_module
            li_dom.appendChild li_dom_path
            li_dom.appendChild li_dom_line
            li_dom.appendChild li_dom_pos
            li_dom.appendChild li_dom_message
            self = @
            li_dom.onclick = (event) -> self.onClickError @id
            ol_dom.appendChild li_dom

        @element.appendChild ol_dom
