
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoInstall
        go_bin: null
        options: null
        target_path: null
        module_path: null
        pkg_path: null

        linter: null

        status_bar: null
        build_count: 0

        constructor: (panel, status_bar) ->
            @errorsView = panel.errors
            @compiler_output = panel.compiler_output
            @panel = panel
            @status_bar = status_bar

        set_configuration: (configuration) ->
            @go_bin = path.join configuration.env["GOROOT"], 'bin', 'go'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
            @module_path = configuration.module_path
            @options.cwd = @module_path
            @target_path = @options.env['TARGET_PATH']
            @pkg_path = @options.env['PKGDIR']
            @save_before_build = configuration.save_before_build

            @status_bar.classList.remove 'hidden'
            @stop_build_status()

        build: () =>
            @run_build_status()
            if @save_before_build
                for editor in atom.workspace.getTextEditors()
                    # grammar = editor.getGrammar()
                    # return if !grammar or grammar.name != 'Go'
                    editor.save()

            console.log 'go install target_path: ', @target_path
            proc = child_process.spawn @go_bin, ['install', '-o', @target_path, '-pkgdir', @pkg_path], @options

            @panel.compiler_output.textContent += @go_bin + ' ' + 'install' + '-i' +
             '-o' + @target_path + '-pkgdir' + @pkg_path + '\n'
            all_str = ""
            stdout = ""
            stderr = ""
            proc.stdout.on 'data', (data) =>
                stdout += data.toString()
                all_str += data.toString()
                @panel.compiler_output.addStr data.toString() if @build_count < 3
                console.log 'go install stdout: ' + data.toString()
            proc.stderr.on 'data', (data) =>
                stderr += data.toString()
                all_str += data.toString()
                @panel.compiler_output.addStr data.toString() if @build_count < 3
                console.log 'go install stderr: ' + data.toString()
            # proc.on 'error', (err) ->
            #     console.log 'gobuild process error ', err
            # proc.on 'message', (message) ->
            #     console.log 'gobuild process message  ', message
            # proc.on 'exit', (code) ->
            #     # console.log 'gobuild process exit with code ', code
            # proc.on 'disconnect', (code) ->
            #     console.log 'gobuild process disconnect ', code
            proc.on 'close', (code) =>
                # @panel.show()
                @parse_errors(all_str)
                @errorsView.setStdOut @errors if @build_count < 3
                @build_count++
                @stop_build_status()
                console.log 'go install process exited with code ', code

        parse_errors: (str) =>
            reg = /(?:#\s(.*?)[\n\r]?|(?:^|[\n\r]))(.+?):(?:(\d+):(\d+):|(\d+):)\s*/gm

            errors_parse = str.split reg
            errors_parse.shift()
            @errors = []
            for i in [0...(errors_parse.length / 6)]
                log_err =
                    module: errors_parse[i * 6 + 0]
                    path: errors_parse[i * 6 + 1]
                    line: errors_parse[i * 6 + 2]
                    pos: errors_parse[i * 6 + 3]
                    message: errors_parse[i * 6 + 5]

                line = errors_parse[i * 6 + 4]
                log_err.line = line if line? && !log_err.line?
                log_err.line = parseInt log_err.line if log_err.line?
                if log_err.pos?
                    log_err.pos = parseInt log_err.pos
                else
                    log_err.pos = 0
                log_err.module = "" if !log_err.module?

                if !path.isAbsolute(log_err.path)
                    log_err.path = path.join @module_path, log_err.path
                @errors.push log_err
            @send_to_linter()

        send_to_linter: () =>
            return if !@linter?
            return if !@errors?
            linter_errors = []

            for err in @errors
                linter_err =
                    type: 'Error'
                    text: err.message
                    range: [[err.line - 1, err.pos], [err.line - 1, err.pos + 1000]]
                    filePath: err.path
                linter_errors.push linter_err
            @linter.setMessages linter_errors

        run_build_status: () ->
            return if !@status_bar?
            while @status_bar.firstChild then @status_bar.removeChild @status_bar.firstChild
            install = document.createElement 'span'
            install.classList.add 'inline-block'
            install.classList.add 'loading'
            install.classList.add 'loading-spinner-tiny'
            @status_bar.appendChild install
            @panel.compiler_output.clear()
            @errorsView.clear()

        stop_build_status: () ->
            return if !@status_bar?
            while @status_bar.firstChild then @status_bar.removeChild @status_bar.firstChild
            install = document.createElement 'span'
            install.classList.add 'inline-block'
            # build.classList.add 'icon'
            # build.classList.add 'icon-playback-play'
            install.textContent = "Install"
            install.addEventListener "click", () =>
                @build()
            @status_bar.appendChild install

        deactivate: ->

    module.exports = GoInstall
