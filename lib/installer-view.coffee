module.exports =
class InstallerView
    constructor: (InstallGoDef, InstallGoGuru, InstallGoCode, InstallGoMobile, CloseInstaller) ->
        @element = document.createElement 'atom-panel'
        @element.classList.add 'modal'

        div_panel = document.createElement 'div'
        div_panel.classList.add 'inset-panel'
        @element.appendChild div_panel

        div_header = document.createElement 'div'
        div_header.classList.add 'panel-heading'
        div_header.textContent = 'AtomGo Installer'
        div_panel.appendChild div_header

        div_content = document.createElement 'div'
        div_content.classList.add 'panel-body'
        div_content.classList.add 'padded'
        div_panel.appendChild div_content

        div_log = document.createElement 'div'
        div_log.classList.add 'inset-panel'
        div_log.classList.add 'padded'
        div_content.appendChild div_log
        div_content.appendChild document.createElement 'br'

        @log = log = document.createElement 'ul'
        log.classList.add 'info-messages'
        log.classList.add 'block'
        div_log.appendChild log

        button_godef = document.createElement 'button'
        button_godef.classList.add 'btn'
        button_godef.classList.add 'btn-primary'
        button_godef.classList.add 'icon'
        button_godef.classList.add 'icon-cloud-download'
        button_godef.classList.add 'inline-block-tight'
        button_godef.textContent = 'Install GoDef'
        button_godef.onclick = () ->
            while log.firstChild then log.removeChild log.firstChild
            InstallGoDef()
        div_content.appendChild button_godef

        button_goguru = document.createElement 'button'
        button_goguru.classList.add 'btn'
        button_goguru.classList.add 'btn-primary'
        button_goguru.classList.add 'icon'
        button_goguru.classList.add 'icon-cloud-download'
        button_goguru.classList.add 'inline-block-tight'
        button_goguru.textContent = 'Install GoGuru'
        button_goguru.onclick = () ->
            while log.firstChild then log.removeChild log.firstChild
            InstallGoGuru()
        div_content.appendChild button_goguru

        button_gocode = document.createElement 'button'
        button_gocode.classList.add 'btn'
        button_gocode.classList.add 'btn-primary'
        button_gocode.classList.add 'icon'
        button_gocode.classList.add 'icon-cloud-download'
        button_gocode.classList.add 'inline-block-tight'
        button_gocode.textContent = 'Install GoCode'
        button_gocode.onclick = () ->
            while log.firstChild then log.removeChild log.firstChild
            InstallGoCode()
        div_content.appendChild button_gocode

        button_gomobile = document.createElement 'button'
        button_gomobile.classList.add 'btn'
        button_gomobile.classList.add 'btn-primary'
        button_gomobile.classList.add 'icon'
        button_gomobile.classList.add 'icon-cloud-download'
        button_gomobile.classList.add 'inline-block-tight'
        button_gomobile.textContent = 'Install GoMobile'
        button_gomobile.onclick = () ->
            while log.firstChild then log.removeChild log.firstChild
            InstallGoMobile()
        div_content.appendChild button_gomobile

        button_close = document.createElement 'button'
        button_close.classList.add 'btn'
        button_close.textContent = 'Close'
        button_close.onclick = () ->
            while log.firstChild then log.removeChild log.firstChild
            CloseInstaller()
        div_content.appendChild button_close

    destroy: =>
        @element.remove()

    getElement: =>
        @element

    logString: (str) =>
        li = document.createElement 'li'
        li.textContent = str
        @log.appendChild li
