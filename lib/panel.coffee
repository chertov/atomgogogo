
    ErrorsView = require './errors-view'
    CompilerOutput = require './compiler_output'

    class Panel
        view: null
        panel: null
        errors: null

        height: 200

        constructor: (status_bar) ->
            status_bar.addEventListener "click", () =>
                if @panel.isVisible()
                    @panel.hide()
                else
                    @panel.show()

            atomgo = document.createElement 'div'
            atomgo.classList.add 'atomgo'

            @atomgo = atomgo

            @header = document.createElement 'div'
            @header.classList.add 'atomgo__header'
            atomgo.appendChild @header
            @header.addEventListener "mousemove", (event) =>
                if event.buttons == 1
                    @height -= event.movementY
                    @height = 100 if @height < 100
                    @height = 700 if @height > 700
                    @recalc_pos()


            columns_wrapper = document.createElement 'div'
            columns_wrapper.classList.add 'atomgo__columns-wrapper'
            atomgo.appendChild columns_wrapper

            column1 = document.createElement 'div'
            column1.classList.add 'atomgo__column'
            column1.innerHTML = "sjdgh lskjdfhg lsjkdfg lksjdfg"
            columns_wrapper.appendChild column1
            column2 = document.createElement 'div'
            column2.classList.add 'atomgo__column'
            column2.innerHTML = "sjdgh lskjdfhg lsjkdfg lksjdfg"
            columns_wrapper.appendChild column2
            column3 = document.createElement 'div'
            column3.classList.add 'atomgo__column'
            column3.innerHTML = "sjdgh lskjdfhg lsjkdfg lksjdfg"
            columns_wrapper.appendChild column3


            @footer = document.createElement 'div'
            @footer.classList.add 'atomgo__footer'
            atomgo.appendChild @footer

            @errors = new ErrorsView column1
            @compiler_output = new CompilerOutput column2
            @recalc_pos()

            @panel = atom.workspace.addBottomPanel(item: atomgo, visible: false)

        recalc_pos: () =>
            new_height = "" + @height + "px"
            @atomgo.style.height = new_height

        show: =>
            @panel.show()
        hide: =>
            @panel.hide()

        deactivate: =>
            @panel.destroy()

    module.exports = Panel
