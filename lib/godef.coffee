
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoDef
        godef_bin: null
        options: null
        alt_down: false
        current_path: ''

        constructor: ->
            document.body.addEventListener "keyup", (event) => @alt_down = event.altKey
            document.body.addEventListener "keydown", (event) => @alt_down = event.altKey

            atom.workspace.observeTextEditors @observeTextEditors

        set_configuration: (configuration) =>
            @godef_bin = path.join configuration.env["GOROOT"], 'bin', 'godef'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
                console.log _var, value
            @options.cwd = configuration.module_path

        godef: (text, offset) =>
            console.log 'godef'
            #ls = child_process.spawn godef_bin, ['-f', filepath, '-o', offset], options
            proc = child_process.spawn @godef_bin, ['-i', '-o', offset], @options
            stdout = ""
            stderr = ""
            proc.stdout.on 'data', (data) ->
                stdout += data.toString()
            proc.stderr.on 'data', (data) ->
                stderr += data.toString()
            proc.on 'close', (code) =>

                console.log 'godef stdout: ', stdout
                if stderr != ''
                    options =
                        detail: stderr
                    atom.notifications.addWarning "GoDef", options
                    return

                params = stdout.match /(?:^\s*(.*):(\d+):(\d+)\s*$)/
                if params?
                    source_path = params[1]
                    row = params[2]
                    column = params[3]
                    # console.log source_path, row, column
                    options =
                        initialLine: row - 1
                        initialColumn: column - 1
                    atom.workspace.open source_path, options
                    return

                params = stdout.match /(?:^\s*(.*):(\d+)\s*$)/
                if params?
                    row = params[1]
                    column = params[2]
                    options =
                        initialLine: row - 1
                        initialColumn: column - 1
                    atom.workspace.open @current_path, options
                    return

                params = stdout.match /(?:^\s*(.*):(\d+)\s*$)/
                console.log 'godef params: ', params

                if stdout != ''
                    options =
                        detail: stdout
                    atom.notifications.addInfo "GoDef", options
                    return


            proc.stdin.setEncoding 'utf-8'
            proc.stdin.write text
            proc.stdin.end()

        observeTextEditors: (editor) =>
            editor.onDidChangeCursorPosition (event) =>
                @on_cursor event, editor

        on_cursor: (event, editor) =>
            return
            return if !@alt_down
            grammar = editor?.getGrammar()
            @current_path = editor?.getPath()
            return if !grammar or grammar.name != 'Go'
            characterOffset = editor.getBuffer().characterIndexForPosition(editor.getCursorBufferPosition())
            fulltext = editor.getText()
            text = fulltext.substring(0, characterOffset)
            offset = Buffer.byteLength(text, "utf8")
            # console.log editor.getTitle() + ": ", editor.getText().substring(characterOffset - 10, characterOffset)
            @godef fulltext, offset

    module.exports = GoDef
