
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'
    InstallerView = require './installer-view'

    class GoInstaller
        go_bin: null

        ShowInstaller: () =>
            @options = {env: process.env}
            if !@options.env['GOROOT']?
                isWin = /^win/.test process.platform
                if isWin
                    @options.env['GOROOT'] = "C:\\Go\\"
                else
                    @options.env['GOROOT'] = "/usr/local/go/"
            @go_bin = path.join @options.env['GOROOT'], 'bin', 'go'
            @options.cwd = @options.env["GOROOT"]
            @options.env["GOPATH"] = @options.env["GOROOT"]

            @installerView = new InstallerView @InstallGoDef, @InstallGoGuru, @InstallGoCode, @InstallGoMobile, @CloseInstaller
            @installerPanel = atom.workspace.addModalPanel(item: @installerView.getElement(), visible: true)

        CloseInstaller: =>
            @installerPanel.destroy()
            @installerView.destroy()

        InstallGoMobile: () =>
            # go get golang.org/x/mobile/cmd/gomobile
            ls = child_process.spawn @go_bin, ['get', 'golang.org/x/mobile/cmd/gomobile'], @options
            @installerView.logString '> go get golang.org/x/mobile/cmd/gomobile'
            ls.stdout.on 'data', (data) =>
                @installerView.logString data
            ls.stderr.on 'data', (data) =>
                @installerView.logString data
            ls.on 'close', (code) =>
                @installerView.logString('go get process exited with code ' + code)
                @installerView.logString '> gomobile init'
                ls = child_process.spawn 'gomobile', ['init'], @options
                ls.stdout.on 'data', (data) =>
                    @installerView.logString data
                ls.stderr.on 'data', (data) =>
                    @installerView.logString data
                ls.on 'close', (code) =>
                    @installerView.logString('gomobile process exited with code ' + code)

        InstallGoCode: () =>
            # go get -v github.com/nsf/gocode
            ls = child_process.spawn @go_bin, ['get', '-v', '-u', 'github.com/nsf/gocode'], @options
            @installerView.logString '> go get -v -u github.com/nsf/gocode'
            ls.stdout.on 'data', (data) =>
                @installerView.logString data
            ls.stderr.on 'data', (data) =>
                @installerView.logString data
            ls.on 'close', (code) =>
                @installerView.logString('go get process exited with code ' + code)

        InstallGoDef: () =>
            # go get -v github.com/rogpeppe/godef
            ls = child_process.spawn @go_bin, ['get', '-v', 'github.com/rogpeppe/godef'], @options
            @installerView.logString '> go get -v github.com/rogpeppe/godef'
            ls.stdout.on 'data', (data) =>
                @installerView.logString data
            ls.stderr.on 'data', (data) =>
                @installerView.logString data
            ls.on 'close', (code) =>
                @installerView.logString('go get process exited with code ' + code)

        InstallGoGuru: () =>
            # go get -v golang.org/x/tools/cmd/guru
            ls = child_process.spawn @go_bin, ['get', '-v', 'golang.org/x/tools/cmd/guru'], @options
            @installerView.logString '> go get -v golang.org/x/tools/cmd/guru'
            ls.stdout.on 'data', (data) =>
                @installerView.logString data
            ls.stderr.on 'data', (data) =>
                @installerView.logString data
            ls.on 'close', (code) =>
                @installerView.logString('go get process exited with code ' + code)

    module.exports = GoInstaller
