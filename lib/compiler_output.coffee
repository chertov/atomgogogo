path = require 'path'

module.exports =
class CompilerOutput
    constructor: (element) ->
        @element = element
        @element.style["white-space"] = 'pre-wrap'
    addStr: (str) =>
        @element.textContent += str
    clear: () =>
        @element.innerHTML = ""
