
    class StatusBar
        build: null             # build button element
        configurations: null    # configurations list, reload config file button

        constructor: () ->
            @div_panel = div_panel = document.createElement 'div'
            div_panel.classList.add 'status-bar-atomgo'
            div_panel.classList.add 'inline-block'

            txt = document.createElement 'span'
            txt.textContent = "Go: "
            div_panel.appendChild txt

            @panel_btn = panel_btn = document.createElement 'span'
            panel_btn.classList.add 'icon'
            panel_btn.classList.add 'icon-chevron-up'
            panel_btn.classList.add 'hidden'
            div_panel.appendChild panel_btn

            selector = document.createElement 'select'
            selector.classList.add 'hidden'
            div_panel.appendChild selector
            el = document.createElement 'span'
            el.textContent = "  "
            div_panel.appendChild el
            reload = document.createElement 'span'
            reload.classList.add 'hidden'
            div_panel.appendChild reload

            @configurations =
                selector: selector
                reload: reload

            # settings = document.createElement 'span'
            # settings.classList.add 'inline-block'
            # settings.classList.add 'icon'
            # settings.classList.add 'icon-gear'
            # settings.addEventListener "click", () =>
            #     @openSetting()
            # div_panel.appendChild settings

            @build = document.createElement 'div'
            @build.classList.add 'inline-block'
            @build.classList.add 'hidden'
            div_panel.appendChild @build

            @install = document.createElement 'div'
            @install.classList.add 'inline-block'
            @install.classList.add 'hidden'
            div_panel.appendChild @install

            @run = document.createElement 'div'
            @run.classList.add 'inline-block'
            @run.classList.add 'hidden'
            div_panel.appendChild @run
            # @run.classList.add 'icon'
            # @run.classList.add 'icon-playback-play'
            # @run.addEventListener "click", () ->
            #     atom.commands.dispatch(atom.views.getView(atom.workspace), "atomgo:run")
            #div_panel.appendChild @run

            div_panel.classList.add 'hidden'

        set_configuration: (configuration) =>
            return if !configuration?
            @div_panel.classList.remove 'hidden'

        set: (statusBar) =>
            @statusBarTile = statusBar.addLeftTile(item: @div_panel, priority: 100)

        deactivate: () ->
            @statusBarTile?.destroy()
            @statusBarTile = null


    module.exports = StatusBar
