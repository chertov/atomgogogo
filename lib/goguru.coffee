
    fs = require 'fs'
    path = require 'path'
    child_process = require 'child_process'

    class GoGuru
        guru_bin: null
        options: null
        alt_down: false
        markers: []

        constructor: ->
            document.body.addEventListener "keyup", (event) => @alt_down = event.altKey
            document.body.addEventListener "keydown", (event) => @alt_down = event.altKey

            atom.workspace.observeTextEditors @observeTextEditors

        set_configuration: (configuration) =>
            @guru_bin = path.join configuration.env["GOROOT"], 'bin', 'guru'
            @options = {env: process.env}
            for _var, value of configuration.env
                @options.env[_var] = value
            @options.cwd = configuration.module_path

        get_editor: () =>
            editor = atom.workspace.getActiveTextEditor()
            grammar = editor.getGrammar()
            return undefined if !grammar or grammar.name != 'Go'
            characterOffset = editor.getBuffer().characterIndexForPosition(editor.getCursorBufferPosition())
            fulltext = editor.getText()
            text = fulltext.substring(0, characterOffset)
            offset = Buffer.byteLength(text, "utf8")
            path = editor.getPath()
            # console.log path + ':#' + offset

            return {editor: editor, offset: offset, path: path}

        clear_markers: () =>
                for marker in @markers
                    marker.destroy()
                @markers = []

        what_close: () =>
            @what_proc = undefined
        what: () =>
            ed = @get_editor()
            return if !ed?
            return if @what_proc?

            # console.log 'guru what start'
            @what_proc = child_process.spawn @guru_bin, ['-json',  'what', ed.path + ':#' + ed.offset], @options
            stdout = ""
            stderr = ""
            @what_proc.stdout.on 'data', (data) ->
                stdout += data.toString()
            @what_proc.stderr.on 'data', (data) ->
                stderr += data.toString()
            @what_proc.on 'close', (code) =>
                if stderr != ''
                    console.log 'guru stderr: ', stderr
                    @what_proc = undefined
                    # @referrers()
                    return @what_close()
                what = JSON.parse stdout
                # console.log 'guru what: ', what
                # return @referrers() if !what.sameids?
                return @what_close() if !what.sameids?
                for same_line in what.sameids
                    params = same_line.match /(?:^\s*(.*):(\d+):(\d+)\s*$)/
                    #console.log same_line, params
                    return @what_close() if !params?
                    return @what_close() if params.length < 4
                    path = params[1]
                    row = parseInt(params[2]) - 1
                    column = parseInt(params[3]) - 1

                    range = [[row, column], [row, column + what.object.length]]
                    # range = [[column, row], [column + what.object.length, row]]
                    #console.log range
                    marker = ed.editor.markBufferRange range
                    type = 'highlight'
                    # decoration = editor.decorateMarker(marker, {type: 'line', class: '#{type}-#{@getRandomColor()}'})
                    decoration = ed.editor.decorateMarker(marker, {type: type, class: "highlight-green"})
                    # decoration()
                    @markers.push marker
                    @what_close()

        referrers: () =>
            ed = @get_editor()
            return if !ed?

            proc = child_process.spawn @guru_bin, ['-json',  'referrers', ed.path + ':#' + ed.offset], @options
            stdout = ""
            stderr = ""
            proc.stdout.on 'data', (data) ->
                stdout += data.toString()
            proc.stderr.on 'data', (data) ->
                stderr += data.toString()
            proc.on 'close', (code) =>
                if stderr != ''
                    console.log 'guru stderr: ', stderr
                    return
                jsons = stdout.split '}\n{'
                # console.log 'guru referrers: ', stdout, jsons

                len = jsons.length
                console.log 'guru referrers len: ', len
                return if len < 2
                for json_str, i in jsons
                    if i == 0 and i < len-1
                        jsons[i] = jsons[i]+'}'
                    if i > 0 and i < len-1
                        jsons[i] = '{'+jsons[i]+'}'
                    if i == len-1
                        jsons[i] = '{'+jsons[i]
                    #console.log i, jsons[i]

                header_json = jsons[0]
                header = JSON.parse header_json
                console.log 'guru referrers header: ', header
                return if !header?

                packages = []
                for i, package_json of jsons
                    if i > 0
                        package_obj = JSON.parse package_json
                        packages.push package_obj
                console.log 'guru referrers packages: ', packages

                for package_obj in packages
                    return if !package_obj.refs?
                    for ref in package_obj.refs
                        params = ref.pos.match /(?:^\s*(.*):(\d+):(\d+)\s*$)/
                        return if !params?
                        return if params.length < 4
                        path = params[1]
                        continue if path != ed.path
                        row = parseInt(params[2]) - 1
                        column = parseInt(params[3]) - 1
                        range = [[row, column], [row, column + 3]]

                        marker = ed.editor.markBufferRange range
                        type = 'highlight'
                        decoration = ed.editor.decorateMarker(marker, {type: type, class: "highlight-green"})
                        @markers.push marker

        definition: () =>
            ed = @get_editor()
            return if !ed?

            proc = child_process.spawn @guru_bin, ['-json',  'definition', ed.path + ':#' + ed.offset], @options
            stdout = ""
            stderr = ""
            proc.stdout.on 'data', (data) ->
                stdout += data.toString()
            proc.stderr.on 'data', (data) ->
                stderr += data.toString()
            proc.on 'close', (code) =>
                if stderr != ''
                    console.log 'guru stderr: ', stderr
                    return
                definition = JSON.parse stdout
                return if !definition?
                return if !definition.objpos?

                params = definition.objpos.match /(?:^\s*(.*):(\d+):(\d+)\s*$)/
                return if !params?
                return if params.length < 4
                path = params[1]
                row = parseInt(params[2]) - 1
                column = parseInt(params[3]) - 1
                options =
                    initialLine: row
                    initialColumn: column
                atom.workspace.open path, options
                return

        whicherrs: () =>
            ed = @get_editor()
            return if !ed?

            proc = child_process.spawn @guru_bin, ['-json', '-scope=...',  'whicherrs', ed.path + ':#' + ed.offset], @options
            stdout = ""
            stderr = ""
            proc.stdout.on 'data', (data) ->
                stdout += data.toString()
            proc.stderr.on 'data', (data) ->
                stderr += data.toString()
            proc.on 'close', (code) =>
                if stderr != ''
                    console.log 'guru whicherrs stderr: ', stderr
                    return
                whicherrs = JSON.parse stdout
                console.log 'guru whicherrs: ', whicherrs

        observeTextEditors: (editor) =>
            editor.onDidChangeCursorPosition (event) =>
                @on_cursor event, editor

        on_cursor: (event, editor) =>
            @clear_markers()

            if @alt_down
                @definition()
            else
                @what()

    module.exports = GoGuru
